//
//  Comment+CoreDataClass.swift
//  CommentsAndComments
//
//  Created by Flávio Silvério on 30/10/16.
//  Copyright © 2016 Flávio Silvério. All rights reserved.
//

import Foundation
import CoreData


public class Comment: NSManagedObject {

    
    class func insert(comments: [Dictionary<String,Any>],
                      inContext context: NSManagedObjectContext,
                      completion: @escaping (
        _ success: Bool,
        _ error: NSError?) -> ()) {
        
        let localContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
        localContext.parent = context
        localContext.mergePolicy = NSMergePolicy.mergeByPropertyObjectTrump
        localContext.perform {
            
            for comment in comments {
                
                let predicate = NSPredicate(format: "commentID == \(comment["id"]!)")
                let currentComment = Comment.findOrCreate(objectWithPredicate: predicate, inManagedObjectContext: localContext) as! Comment
                
                currentComment.commentID = Int16(comment["id"] as! Int)
                currentComment.name      = comment["name"]     as! String?
                currentComment.email     = comment["email"]    as! String
                currentComment.body      = comment["body"]     as! String
                
                currentComment.post      = Post.findOrCreate(objectWithPredicate: NSPredicate(format: "postID == \(comment["postId"]!)"),
                                                                 inManagedObjectContext: localContext) as! Post

            }
            
            do {
                try localContext.save()
                try localContext.parent?.save()
                completion(true, nil)
            } catch let error as NSError {
                completion(false, error)
            }
        }
    }
    
}
