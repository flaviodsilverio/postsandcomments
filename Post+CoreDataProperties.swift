//
//  Post+CoreDataProperties.swift
//  PostsAndComments
//
//  Created by Flávio Silvério on 02/11/16.
//  Copyright © 2016 Flávio Silvério. All rights reserved.
//

import Foundation
import CoreData

extension Post {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Post> {
        return NSFetchRequest<Post>(entityName: "Post");
    }

    @NSManaged public var body: String?
    @NSManaged public var postID: Int16
    @NSManaged public var title: String?
    @NSManaged public var comments: NSSet?
    @NSManaged public var user: User

}

// MARK: Generated accessors for comments
extension Post {

    @objc(addCommentsObject:)
    @NSManaged public func addToComments(_ value: Comment)

    @objc(removeCommentsObject:)
    @NSManaged public func removeFromComments(_ value: Comment)

    @objc(addComments:)
    @NSManaged public func addToComments(_ values: NSSet)

    @objc(removeComments:)
    @NSManaged public func removeFromComments(_ values: NSSet)

}
