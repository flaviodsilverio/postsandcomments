//
//  User+CoreDataClass.swift
//  PostsAndComments
//
//  Created by Flávio Silvério on 29/10/16.
//  Copyright © 2016 Flávio Silvério. All rights reserved.
// TEST

import Foundation
import CoreData


public class User: NSManagedObject {
    
    class func insert(users: [Dictionary<String,Any>],
                      inContext context: NSManagedObjectContext,
                      completion: @escaping ((
        _ success: Bool,
        _ error: NSError? ) -> ())){
        
        
        let localContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
        localContext.parent = context
        localContext.mergePolicy = NSMergePolicy.mergeByPropertyObjectTrump
        localContext.perform {
            
            for user in users {
                
                let predicate = NSPredicate(format: "userID == \(user["id"]!)")
                let currentUser = User.findOrCreate(objectWithPredicate: predicate, inManagedObjectContext: localContext) as! User
                
                currentUser.userID = Int16(user["id"]   as! Int)
                currentUser.address  = user["address"]  as! NSObject?
                currentUser.company  = user["company"]  as! NSObject?
                currentUser.email    = user["email"]    as! String?
                currentUser.name     = user["name"]     as! String?
                currentUser.phone    = user["phone"]    as! String?
                currentUser.username = user["username"] as! String?
                currentUser.website  = user["website"]  as! String?
                
            }
            
            do {
                try localContext.save()
                try localContext.parent?.save()
                completion(true, nil)
            } catch let error as NSError {
                completion(false, error)
            }
            
        }
        
    }
    
}
