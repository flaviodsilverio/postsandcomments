//
//  Comment+CoreDataProperties.swift
//  PostsAndComments
//
//  Created by Flávio Silvério on 02/11/16.
//  Copyright © 2016 Flávio Silvério. All rights reserved.
//

import Foundation
import CoreData

extension Comment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Comment> {
        return NSFetchRequest<Comment>(entityName: "Comment");
    }

    @NSManaged public var body: String
    @NSManaged public var commentID: Int16
    @NSManaged public var email: String
    @NSManaged public var name: String?
    @NSManaged public var post: Post

}
