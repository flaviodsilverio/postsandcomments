//
//  Post+CoreDataClass.swift
//  PostsAndComments
//
//  Created by Flávio Silvério on 30/10/16.
//  Copyright © 2016 Flávio Silvério. All rights reserved.
//

import Foundation
import CoreData


public class Post: NSManagedObject {

    class func insert(posts: [Dictionary<String,Any>],
                      inContext context: NSManagedObjectContext,
                      completion: @escaping (
        _ success: Bool,
        _ error: NSError? ) -> ()){
        
        let localContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
        localContext.parent = context
        localContext.mergePolicy = NSMergePolicy.mergeByPropertyObjectTrump
        localContext.perform {
        
            for post in posts {
                
                let predicate = NSPredicate(format: "postID == \(post["id"]!)")
                let currentPost = Post.findOrCreate(objectWithPredicate: predicate, inManagedObjectContext: localContext) as! Post
                
                currentPost.postID = Int16(post["id"] as! Int)
                currentPost.title  = post["title"]    as! String?
                currentPost.body   = post["body"]     as! String?
                
                let postPredicate = NSPredicate(format: "userID == \(post["userId"]!)")
                currentPost.user   = User.findOrCreate(objectWithPredicate: postPredicate, inManagedObjectContext: localContext) as! User
            }
            
            do {
                try localContext.save()
                try localContext.parent?.save()
                completion(true, nil)
            } catch let error as NSError {
                completion(false, error)
            }
        }
    }
    
}
