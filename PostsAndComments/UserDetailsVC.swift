//
//  UserDetailsVC.swift
//  PostsAndComments
//
//  Created by Flávio Silvério on 29/10/16.
//  Copyright © 2016 Flávio Silvério. All rights reserved.
//

import UIKit
import MessageUI

class UserDetailsVC: UITableViewController, MFMailComposeViewControllerDelegate {

    var user : User!
    
    enum sections : Int {
        case details   = 0
        case postsInfo
        case contacts
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = user.username
        self.tableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        
        cell.detailTextLabel?.text =  details(forCellAtIndexPath: indexPath)

        return cell
    }
    
    func details(forCellAtIndexPath indexPath: IndexPath) -> String{
    
        switch indexPath.section {
            
        case sections.details.rawValue:
            switch indexPath.row {
            case 0:
                return user.username!
            case 1:
                guard let companyDetails = (user.company as? Dictionary<String, Any>) else {
                    return "N/A"
                }
                return companyDetails["name"] as! String? ?? "N/A"
            default: break
            }
            break
            
        case sections.postsInfo.rawValue:
            switch indexPath.row {
            case 0:
                return String(user.posts!.count)
            case 1:
                return "0"
            default: break
            }
            
            break
        case sections.contacts.rawValue:
            switch indexPath.row {
            case 0:
                return user.website ?? "N/A"
            case 1:
                return user.email ?? "N/A"
            case 2:
                return user.phone ?? "N/A"
            default: break
            }
            break
        default:
            
            break
            
        }
        
        return ""
        
    }
    
    //MARK: Actions
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == sections.contacts.rawValue {
        
            switch indexPath.row {
            case 0:
                openWebsite()
            case 1:
                emailUser()
            case 2:
                phoneUser()
            default: break
            }
            
        }
    }
    
    //MARK: Website
    
    func openWebsite(){
        UIApplication.shared.open(URL(string: "http://www.google.co.uk")!)
    }
    
    //MARK: Email
    
    func emailUser() {
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        }
        
    }
    
    //MARK: Phone call
    
    func phoneUser() {
        
        let alert = UIAlertController(title: "", message: "Would you like to call \(user.phone)?", preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: {
            (action) in
            alert.dismiss(animated: true, completion: {})
        })
        
        let okAction = UIAlertAction(title: "Call", style: UIAlertActionStyle.default, handler: {
            (action) in
            self.callNumber()
        })
        
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: {})
    }
    
    func callNumber(){

        guard let string = user.phone?.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined() else {
            
            return
        }
        
        let phone = "tel://" + string
        let url = URL(string: phone)
        UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients(["flaviodsilverio@gmail.com"])
        return mailComposerVC
    }
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        
    }
    
}
