//
//  APIConnection.swift
//  PostsAndComments
//
//  Created by Flávio Silvério on 29/10/16.
//  Copyright © 2016 Flávio Silvério. All rights reserved.
//

import UIKit
import CoreData

protocol APIConnectionDelegate {

    func didReceiveDataSuccessfully()
    func didOccurProblem(errorString error: String)
    
}

class APIConnection: NSObject {

    var delegate: APIConnectionDelegate?
    var basePath: String?
    var managedObjectContext : NSManagedObjectContext?

    func getAllData(){
        
        self.get(dataFromEndpoint: "users")

    }
    
    /*
     I've opted to get all the data sequentially, since in this particular scenario a Post needs to belong to a user, and comments must be linked to a post. So, in my opinion, it made sense to have everything sequentially.
     
     That being said, since expandability was a concern, there's still coredata helper methods that'll do a "3 step fetch" as a performance concern. First they'll lool for the objects in the managedObjectContext loaded objects, then they try to fetch them from  the local database and finnaly, if there's no object with said ID, it'll be created.
     
     */
    
    private func get(dataFromEndpoint endPoint: String){
    
        get(endPoint: endPoint) {
            [weak self] (success, data, error) in
        
            if success {
            
                let dataToSave = data as! [Dictionary<String, Any>]
                
                switch endPoint {
                    
                case "posts":
                    Post.insert(posts: dataToSave,
                                inContext: (self?.managedObjectContext)!,
                                completion: {
                                    (success, error) in
                                    
                                    if success {
                                        self?.get(dataFromEndpoint: "comments")
                                    } else {
                                        self?.didOccurProblem(errorString: error!.localizedDescription)
                                    }
                    })
                case "users":
                    User.insert(users: dataToSave,
                                inContext: (self?.managedObjectContext)!,
                                completion: {
                                    (success, error) in
                                    
                                    if success {
                                        self?.get(dataFromEndpoint: "posts")
                                    } else {
                                        self?.didOccurProblem(errorString: error!.localizedDescription)
                                    }
                    })
                case "comments":
                    Comment.insert(comments: dataToSave,
                                inContext: (self?.managedObjectContext)!,
                                completion: {
                                    (success, error) in
                                    
                                    if success {
                                        
                                        self?.didReceiveDataSuccessfully()
                                    } else {
                                        self?.didOccurProblem(errorString: error!.localizedDescription)
                                    }
                    })
                    
                default:
                    
                    self?.didReceiveDataSuccessfully()
                    
                    break
                    
                }
            
            } else {
            
                self?.didOccurProblem(errorString: error!)
            
            }
        
        }
        
    }
    
    func get(endPoint: String, completion: @escaping ((
        _ success: Bool,
        _ data: Any?,
        _ error: String? ) -> ()))  {
        
        guard (basePath != nil) else {
            return
        }
        
        /*
         Why use "URLSession.shared.dataTask" instead of having a class poperty setup initially and then reuse it accordingly?
         This is a very good question and usually I'd go with the class property, however, since there's no custom configuration, I thought it'd only bring more complexity (or worse understanding) if there was only a class property that'd 
         still be using the shared URLSession.
         */
        
        URLSession.shared.dataTask(with: URL(string: "\(basePath!)\(endPoint)")!) {
            data, response, error in
            
            guard let httpResponse = response as? HTTPURLResponse else {
                
                completion(false, nil, "\(error!.localizedDescription)")
                
                return
            }
            
            switch httpResponse.statusCode {
                
            case 200:
                do {
                
                    let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
                    completion(true, jsonData, nil)
                    
                } catch(let error as NSError){
                    completion(false, nil, "\(error.localizedDescription)")
                }
                
                break
            default:
                completion(false, nil, "Response Status Code: \(httpResponse.statusCode)")
                break
            }
            
        }.resume()

    }
    
    //MARK - Class Delegate Methods
    
    func didReceiveDataSuccessfully(){
        self.delegate?.didReceiveDataSuccessfully()
    }
    
    /*
     The name of the method might not be ideal, however it was meant to be a general name to a method that will be called in any unsuccessful operation, be it coredata, api or any other in this particular project.
     */
    
    func didOccurProblem(errorString error: String) {
        self.delegate?.didOccurProblem(errorString: error)
    }
}
