//
//  PostListVC.swift
//  PostsAndComments
//
//  Created by Flávio Silvério on 29/10/16.
//  Copyright © 2016 Flávio Silvério. All rights reserved.
//

import UIKit
import CoreData

class PostListVC: UITableViewController, APIConnectionDelegate, NoDataViewDelegate {

    var apiManager : APIConnection = APIConnection()
    var allPosts : [Post]?
    var managedObjectContext : NSManagedObjectContext?
    var noData : NoDataView = NoDataView()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        refresh.attributedTitle = NSAttributedString(string: "Refreshing...")
        self.refreshControl = refresh
        self.tableView.addSubview(refresh)
        
        self.tableView.estimatedRowHeight = 80
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.tableFooterView = UIView()
        
        apiManager = APIConnection()
        apiManager.delegate = self
        apiManager.basePath = "http://jsonplaceholder.typicode.com/"
        apiManager.managedObjectContext = managedObjectContext
        allPosts = Post.fetchAll(inContext: managedObjectContext!) as? [Post]
        
        noData = Bundle.main.loadNibNamed("NoDataView", owner: self, options: nil)?.first as! NoDataView
        noData.frame = self.tableView.frame
        noData.layoutIfNeeded()
        noData.delegate = self
        self.tableView.addSubview(noData)
        self.tableView.sendSubview(toBack: noData)
        
        update()
        
    }
    

    //MARK: Updating the Data
    func update(){
        
        if allPosts?.count == 0 {
            
            self.tableView.bringSubview(toFront: noData)
            noData.set(isRefreshing: true)
            
        }
        
        apiManager.getAllData()
        
    }
    
    //MARK: Table View delegate Methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard allPosts != nil else {
            return 0
        }
        
        return allPosts!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! PostCell
        
        let currentPost = allPosts?[indexPath.row]
        
        cell.postTitle.text  = currentPost?.title
        cell.postAuthor.text = currentPost?.user.username
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showPostDetails", sender: self)
    }
    
    //MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        let index = tableView.indexPathForSelectedRow?.row
        let destination = segue.destination as! PostDetailsVC
        destination.post = (allPosts?[index!])!
    }
    
    
    //MARK: Delegation Methods
    
    func didReceiveDataSuccessfully() {
        
        allPosts = Post.fetchAll(inContext: managedObjectContext!) as? [Post]
        
        DispatchQueue.main.sync {

            self.tableView.refreshControl?.endRefreshing()
            
            if allPosts?.count == 0 {
                self.tableView.bringSubview(toFront: noData)
                noData.set(isRefreshing: false)
            } else {
                noData.activityIndicator.stopAnimating()
                self.tableView.sendSubview(toBack: noData)
            }
            
            self.tableView.reloadData()
        }
        
    }
    
    func didOccurProblem(errorString error: String) {
        
        let alertController = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        DispatchQueue.main.sync {
            
            self.tableView.refreshControl?.endRefreshing()
            
            self.present(alertController, animated: true, completion: {
                
                if self.allPosts?.count == 0 {
                    self.tableView.bringSubview(toFront: self.noData)
                    self.noData.set(isRefreshing: false)
                }
                
            })
        }

    }
    
    //MARK: Actions
    
    @IBAction func refresh(_ sender: AnyObject?) {
        self.update()
    }

    func didTapRefreshButton(){
        self.update()
    }
    
}

