//
//  CommentCell.swift
//  PostsAndComments
//
//  Created by Flávio Silvério on 02/11/16.
//  Copyright © 2016 Flávio Silvério. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {

    @IBOutlet var username: UILabel!
    @IBOutlet var comment: UILabel!
    

}
