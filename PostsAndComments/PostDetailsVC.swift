//
//  PostDetailsVC.swift
//  PostsAndComments
//
//  Created by Flávio Silvério on 29/10/16.
//  Copyright © 2016 Flávio Silvério. All rights reserved.
//

import UIKit

class PostDetailsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyTextView: UITextView!
    
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    var post : Post!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text    = post.title
        bodyTextView.text  = post.body
        usernameLabel.text = post.user.username
        
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableViewAutomaticDimension

    }

    //MARK: Table View Datasource and Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let numberOfComments = post.comments?.count else {
            return 0
        }
        
        return numberOfComments
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CommentCell
        
        let commentsArray  = post.comments!.allObjects
        cell.username.text = "\((commentsArray[indexPath.row] as! Comment).name ?? "Unregistered User"):"
        cell.comment.text  = (commentsArray[indexPath.row]    as! Comment).body
        
        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        guard  ((post.comments?.count) != nil) else {
            return "No Comments"
        }
        
        return "Comments: \(post.comments!.count)"
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier  == "showUserDetails" {
            let destination  = segue.destination as! UserDetailsVC
            destination.user = post.user
        }
        
    }
    
    //MARK: - Actions
    
    @IBAction func usernameDetailsTapped(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "showUserDetails", sender: self)
    }

    @IBAction func shareButtonTapped(_ sender: AnyObject) {
        
        let text = post.body
        let google : URL = URL(string: "www.google.co.uk")!
        
        let activityVC = UIActivityViewController(activityItems: [text,google], applicationActivities: nil)
        activityVC.excludedActivityTypes = []
        
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    
    
}
