//
//  ManagedObject+Extensions.swift
//  PostsAndComments
//
//  Created by Flávio Silvério on 31/10/16.
//  Copyright © 2016 Flávio Silvério. All rights reserved.
//

import CoreData

extension NSManagedObject {
    
    class var entityName: String! {
        get {
            return self.entity().managedObjectClassName.components(separatedBy: ["."]).last!
        }
    }
    
    public static func findOrCreate(objectWithPredicate predicate: NSPredicate, inManagedObjectContext moc: NSManagedObjectContext) -> NSManagedObject{
        
        //First we should fetch an existing object in the context as a performance optimization
        guard let existingObject = moc.find(objectMatchingPredicate: predicate, forEntityWithName: entityName) else {
            
            return fetchOrCreate(objectMatchingPredicate: predicate, inContext: moc)
            
        }
        
        return existingObject
        
    }
    
    public static func fetchOrCreate(objectMatchingPredicate predicate: NSPredicate, inContext moc: NSManagedObjectContext) -> NSManagedObject{
        
        //if it's not in memory, we should execute a fetch to see if it exists
        let fetchRequest = self.fetchRequest()
        fetchRequest.predicate = predicate
        fetchRequest.fetchLimit = 1
        fetchRequest.returnsObjectsAsFaults = false
        
        do {
            
            let objects = try moc.fetch(fetchRequest)
            
            if objects.count > 0 {
                return objects.first as! NSManagedObject
            }
            
        } catch let error {
            print(error)
        }
        
        //If it didn't exist in memory and wasn't fetched, we should create a new object
        return createObject(inContext: moc)
        
    }
    
    public static func createObject(inContext moc: NSManagedObjectContext) -> NSManagedObject{
        let newObject = NSEntityDescription.insertNewObject(forEntityName: entityName , into: moc)
        return newObject
    }
    
    public static func fetchAll(inContext moc: NSManagedObjectContext) -> [NSManagedObject]{
        
        let fetch = self.fetchRequest()
        
        do {
            
            let objects = try moc.fetch(fetch)
            
            return objects as! [NSManagedObject]
            
        } catch let error {
            print(error)
        }
        
        return []
    }
}

extension NSManagedObjectContext {
    
    public func find(objectMatchingPredicate predicate: NSPredicate,
                     forEntityWithName entityName: String) -> NSManagedObject? {
        
        for object in self.registeredObjects where
            !object.isFault && (object.entity.name?.contains(entityName))! {
                
            if predicate.evaluate(with: object) {
                return object
            }
                
        }
        
        return nil
    }
    
}
