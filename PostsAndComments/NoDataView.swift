//
//  NoDataView.swift
//  PostsAndComments
//
//  Created by Flávio Silvério on 01/11/16.
//  Copyright © 2016 Flávio Silvério. All rights reserved.
//

import UIKit

protocol NoDataViewDelegate {
    func didTapRefreshButton()
}

class NoDataView: UIView {

    var delegate : NoDataViewDelegate?
    
    @IBOutlet var textLabel: UILabel!
    @IBOutlet var refreshButton: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!

    func set(isRefreshing: Bool){
        
        if isRefreshing {
            
            activityIndicator.isHidden = false
            activityIndicator.startAnimating()
            refreshButton.isHidden = true
            textLabel.text = "Refreshing..."
            
        } else {
            
            activityIndicator.isHidden = true
            activityIndicator.stopAnimating()
            refreshButton.isHidden = false
            textLabel.text = "No data to Show"
            
        }
        
    }
    
    @IBAction func refreshButtonTap(_ sender: AnyObject) {
        
        self.delegate?.didTapRefreshButton()
    }
}
